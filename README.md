# TimeLog

Simple Time Tracking

## Usage

```bash
./timelog start 'Task name/description'
./timelog stop
./timelog report
```
