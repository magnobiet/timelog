#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import datetime
from functools import wraps
from contextlib import closing, contextmanager
import json
import os
import logging
import re
import six
import pytz

BASIC_FORMAT = "%(asctime)s|%(levelname)s|%(name)s|%(funcName)s|%(message)s"
log = logging.getLogger('timelog')
filename = '_timelog.json'

filename = os.path.basename(os.getcwd()) + '.timelog'

class ParamError(Exception):
    pass

class SafeError(Exception):
    pass

class SimpleError(Exception):
    pass

class DateTimeNotInUTC(SimpleError):
    pass

class EmptyData(SimpleError):
    pass

class NotStarted(SimpleError):
    pass

class AlreadyStarted(SimpleError):
    pass

class AlreadyStopped(SimpleError):
    pass

def format_exception(e):
    out = ''
    for arg in e.args:
        if not isinstance(arg, basestring):
            arg = repr(arg)
        out += '|' + arg
    return out[1:]

################################################################################

def localtime():
  return pytz.tzfile.build_tzinfo('localtime', open('/etc/localtime'))

def localize(dt, tzinfo=localtime()):
  try:
    return tzinfo.localize(dt)
  except Exception as e:
    logging.exception(e)
    return dt

def aware(dt):
    return localize(dt, pytz.utc)

################################################################################

def seconds_to_hours(value):
    seconds = int(value) or 0
    minutes, seconds = divmod(seconds, 60)
    hours, minutes = divmod(minutes, 60)
    return hours, minutes, seconds

def seconds_display(value):
    hours, minutes, seconds = seconds_to_hours(value)
    return '%02dh%02dm' % (hours, minutes)
#    return '%02dh%02dm%02ds' % (hours, minutes, seconds)

################################################################################

def aslocaltime(dt):
  return dt.astimezone(localtime())

def asutc(dt):
  return dt.astimezone(pytz.utc)

def asdate(ts):
    ts = ts.replace(hour=0, minute=0, second=0, microsecond=0)
    return ts

def utcnow():
    timestamp = datetime.datetime.utcnow()
    timestamp = timestamp.replace(tzinfo=pytz.utc)
    timestamp = timestamp.replace(second=0, microsecond=0)
    return timestamp

def utcnow_str():
#    return utcnow()
    timestamp = utcnow()
    timestamp = timestamp.isoformat()
    if timestamp.endswith('+00:00'):
        timestamp = timestamp[:-6] + 'Z'
    else:
        raise DateTimeNotInUTC(timestamp)
    return timestamp

# https://github.com/django/django/blob/master/django/utils/dateparse.py

datetime_re = re.compile(
    r'(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})'
    r'[T ](?P<hour>\d{1,2}):(?P<minute>\d{1,2})'
    r'(?::(?P<second>\d{1,2})(?:\.(?P<microsecond>\d{1,6})\d{0,6})?)?'
    r'(?P<tzinfo>Z|[+-]\d{2}(?::?\d{2})?)?$'
)

def parse_datetime(value):
    match = datetime_re.match(value)
    if match:
        kw = match.groupdict()
        if kw['microsecond']:
            kw['microsecond'] = kw['microsecond'].ljust(6, '0')
        tzinfo = kw.pop('tzinfo')
        if tzinfo == 'Z':
            tzinfo = pytz.utc
        else:
            raise DateTimeNotInUTC(value)
#        elif tzinfo is not None:
#            offset_mins = int(tzinfo[-2:]) if len(tzinfo) > 3 else 0
#            offset = 60 * int(tzinfo[1:3]) + offset_mins
#            if tzinfo[0] == '-':
#                offset = -offset
#            tzinfo = get_fixed_timezone(offset)
        kw = {k: int(v) for k, v in six.iteritems(kw) if v is not None}
        kw['tzinfo'] = tzinfo
        timestamp = datetime.datetime(**kw)
#        timestamp = timestamp.replace(microsecond=0)
        return timestamp

def safe(func):
    @wraps(func)
    def inner(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except (IOError, ValueError, TypeError) as e:
            params = [func, e, args, kwargs]
            raise SafeError(*params)
        except (SimpleError) as e:
            params = [func.__name__, e]
            raise SimpleError(*params)
    return inner

@safe
def store(data):
    if data:
        data = json.dumps(data, indent=4)
        with closing(open(filename, 'w')) as f:
            f.write(data)

@safe
def load():
    if not os.path.exists(filename):
        return []
    else:
        with closing(open(filename, 'r')) as f:
            return json.load(f)

def session(func):
    @safe
    @wraps(func)
    def inner(*args, **kwargs):
        data = load()
        data = func(data, *args, **kwargs)
        store(data)

    return inner

def last(data):
    if not data:
        raise EmptyData
    return data[-1]

class Index(object):
    TAG = 0
    START = 1
    STOP = 2

class get(object):
    @classmethod
    def tag(cls, record):
        try:
            return record[Index.TAG]
        except IndexError:
            return None
    @classmethod
    def start(cls, record):
        try:
            return parse_datetime(record[Index.START])
        except IndexError:
            return None
    @classmethod
    def stop(cls, record):
        try:
            return parse_datetime(record[Index.STOP])
        except IndexError:
            return None
    @classmethod
    def raw(cls, record):
        return cls.tag(record), cls.start(record), cls.stop(record)
    @classmethod
    def str(cls, record):
        tag, start, stop = cls.raw(record)
        tag, start, stop = tag or '', start or '', stop or ''
        return str(tag), str(start), str(stop)

class add(object):
    @classmethod
    def start(cls, record, tag, start):
        record.append(tag)
        record.append(start)

    @classmethod
    def stop(cls, record, stop):
        record.append(stop)

def stopped(tail):
    return tail[Index.STOP:]

def started(tail):
    return not stopped(tail)

def can_start(data):
    if data:
        tail = last(data)
        if started(tail):
            raise AlreadyStarted
    data.append([])
    return last(data)

def can_stop(data):
    tail = last(data)
    if not tail:
        raise NotStarted
    if stopped(tail):
        raise AlreadyStopped
    return tail

@session
def start(data, tag=None):
    if not tag:
        raise ParamError

    tail = can_start(data)
    add.start(tail, tag, utcnow_str())
    tag, start, stop = get.raw(tail)
    print format_range(tag, start, start, start - start, '[starting]')
    return data

@session
def stop(data):
    tail = can_stop(data)
    add.stop(tail, utcnow_str())
    tag, start, stop = get.raw(tail)
    print format_range(tag, start, stop, stop - start, '[stopping]')
    return data

def format_range(tag, start, stop, total_time, flag=''):
    start, stop = aslocaltime(start), aslocaltime(stop)
    if flag == 'sum':
        date = '%s' % (start.date())
        time = '%s ' % (stop.date())
        flag = ''
    else:
        date = start.date()
        time = '{0:%H:%M}-{1:%H:%M}'.format(start.time(), stop.time())
    return '%s  %s  %s  %s %s' % (date, time, seconds_display(total_time.total_seconds()), tag, flag)

def tag_filter(tag):
    return tag.split(':', 1)[0]

@session
def report(data, filter='', usage=False):
    if not data:
        if usage:
            return
        raise EmptyData

    range_list = []

    first = None
    last = None

    total_time = datetime.timedelta(0)
    for value in data:
        flag = ''
        tag, start, stop = get.raw(value)

        if filter and tag_filter(tag) != filter:
            continue

        if not first:
            first = start

        if not stop:
            stop = utcnow()
            flag = '[open]'

        last = stop
        total_time += (stop - start)

        if aslocaltime(start).date() == aslocaltime(stop).date():
            range_list.append(format_range(tag, start, stop, stop - start, flag))
        else:
            split = asutc(asdate(aslocaltime(stop)))
            range_list.append(format_range(tag, start, split, split - start, flag))
            range_list.append(format_range(tag, split, stop, stop - split, flag))

    range_list.append('')
    range_list.append(format_range('sum', first, last, total_time, 'sum'))

    if usage:
        range_list = range_list[-3:]

    for item in range_list:
        print item

@session
def list_tags(data):
    tag_list = set()

    for value in data:
        tag_list.add(tag_filter(get.tag(value)))

    for tag in tag_list:
        print tag

def usage():
    print 'usage: %s [start "tag: Info"|stop|tags|report [tag]]' % (sys.argv[0])
#    print
#    report(usage=True)

options = {
    'start': start,
    'stop': stop,
    'report': report,
    'tags': list_tags,
}

def option(params):
    if not params:
        raise ParamError

    param = params[0]
    if not param in options:
        raise ParamError

    options[param](*params[1:])

def main():
    logging.basicConfig(format=BASIC_FORMAT, level=logging.INFO)
    try:
        option(sys.argv[1:])
    except (SafeError, SimpleError) as e:
        log.error(format_exception(e))
    except (ParamError):
        usage()

if __name__ == '__main__':
    main()
